# My PySpark Projects

Este es mi /fork/ privado en Bitbucket del repo `kpassapk/cs38-fall18-projects`. Utiliza un Vagrant box llamado "pyspark-box". 

## 1. Generar "pyspark-box"

```

   $ vagrant plugin install vagrant-share
  
   $ git clone git@github.com:kpassapk/vagrant-spark
   
   $ cd vagrant-spark
   
   $ vagrant up
   
   $ vagrant package
   
```

## 2. Correr "1_hello_world" localmente

Clonar mi /fork/:

```

    $ git clone https://bitbucket.org/username/cs38-fall18-projects
    
    $ cd cs38-fall18-projects

```


Editar el Vagrantfile, cambiando el directorio de proyectos (actualmente `/Users/kyle/src/org/bitbucket/kpassapk/cs38-fall18-projects`) a el directorio donde está clonado _este repositorio_.

Luego, cargar la máquina:

```

    $ vagarant up
    $ vagrant ssh

```

El prompt verde debería decirles que están en Ubuntu Xenial, como usuario "vagrant". 

```
    $ cd /vagrant
    $ pyspark
```

Copiar el código en `1_hello_world` a la consola. (Copy-paste.) El resultado debería ser el siguiente.

```
    3.14154496
``` 

Luego, correr Jupyter notebook (adentro de la máquina virtual.)

```
    $ cd /vagrant
    
    $ jupyter notebook --no-browser

    [I 12:37:32.809 NotebookApp] Writing notebook server cookie secret to /run/user/1000/jupyter/notebook_cookie_secret

    [I 12:37:33.231 NotebookApp] Serving notebooks from local directory: /home/vagrant
    [I 12:37:33.231 NotebookApp] The Jupyter Notebook is running at:
    [I 12:37:33.231 NotebookApp] http://localhost:8888/?token=c6a3be6b1a0e8fac5d7ae6aede45ab9569e1fdba8a2ca43c
    [I 12:37:33.231 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
    [C 12:37:33.232 NotebookApp]

        Copy/paste this URL into your browser when you connect for the first time,
        to login with a token:
            http://localhost:8888/?token=c6a3be6b1a0e8fac5d7ae6aede45ab9569e1fdba8a2ca43c
```

Hacer click en ese URL, cambiando el puerto de 8888 a 8890. (Ver "troubleshooting", abajo, si la URL no carga.) Luego, navegar a la carpeta `1_hello_world` y abrir un notebook de Python3. Ingresar el siguiente texto en la primera celda:

```
    import findspark
    findspark.init()

    import pyspark

    sc = pyspark.SparkContext(appName="Pi")
    num_samples = 100000000

```

Ejecutar la primera celda, ya sea con "run" en el toolbar, o con Ctrl+[Enter]. Luego, crear una segunda celda abajo y escribir lo siguiente:


```
    %load hello_world.py

```

Esto va a cargar el contenido de el archivo de Python al notebook:

```
    import random
    def inside(p):     
      x, y = random.random(), random.random()
      return x*x + y*y < 1

    count = sc.parallelize(range(0, num_samples)).filter(inside).count()

    pi = 4 * count / num_samples
    print(pi)

    sc.stop()

```

Apachar Ctrl+[enter] para evaluar la celda. El resultado debería salir así:

![screenshot](./img/screenshot_2018-07-30_07-53-21.png)

## 3. Hacer un commit y enviarlo como submission

El submission se hace a través de un *pull request* a el branch con tu nombre del repositorio `kpassapk/cs38-fall18-projects`. Una vez estás conforme con tu solución, haz un commit y envíalo a tu propio fork. 

```

    git add .
    git commit -m "add first assignment"
    git push -u origin master

```

Ahora, crear un submission, haciendo un "pull request". Esto lo vamos a hacer con un script, que utiliza git internamente. Primero, configurar git adentro de la máquina virtual, como a continuación:

```
    vagrant ssh

    git config --global credential.helper cache

    git config --global user.name "Mi nombre"
    git config --global user.email "mi.correo@ufm.edu"

```

Ahora, correr el submission script,

```

    cd /vagrant
    ./submit.py 1_hello_world/hello_world.ipynb

```

apuntándole a el .ipynb que creamos anteriormente. El programa crea un commit en el branch "students/{nombre}**, con un README file correspondiente a el encabezado de el Jupyter Notebook.

Luego, en Bitbucket, crear un "pull request" a "kpassapk/cs38-fall18-projects", seleccinoando el branch con tu nombre tanto en el origen como en el destino.

**Notas**: 

1. Es importante correr esto desde adentro de el Vagrant box, y desde el directorio /vagrant.
2. Si estás corriendo una versión anterior de el box, corre el siguiente comando antes de `submit.sh`: (adentro de el Vagrant box)

```
    sudo apt-get install jq
```

## 4. Obtener ejercicios nuevos

Así como usas git para enviar soluciones, también lo usarás para recibir instrucciones y data relacionada a ejercicios nuevos.

Los ejercicios nuevos serán publicados a `kpassapk/cs38-fall18-projects`. A este repositorio le llamaremos "upstream."  Para bajar los ejercicios, se hace un "fetch" y "merge" de este /upstream/. Primero tenemos que identificar la ubicación del `upstream`:

```

  git remote add upstream git@bitbucket.org:kpassapk/cs38-fall18-projects.git

```

Esto se hace solo una vez. Cada vez que se te notifique que hay ejercicios nuevos, los obtendrás ejeutando:

```

  git fetch

  git merge upstream/master

```

([fuente](https://www.atlassian.com/git/articles/git-forks-and-upstreams))



(Estos son los "comandos feos")

## 4. Hacer un share (qué cool!)

Para compartir notebooks entre ustedes, read/write:

```
    $ vagrant share -p 8888

```

Luego visitar el URL, e ingresar el token que aparece en el Vagrant Share de `jupyter notebook`.

```

    $ http://88472da2.ngrok.io/

```

## El "yoga" de este proyecto

Los 10 pasos que se hacen todos los días son:

```

1. git fetch upstream
2. git merge upstream/master
3. vagrant up
4. vagrant ssh (o "comandos feos", arriba)
5. cd /vagrant
6. jupyter notebook --ip=0.0.0.0
7. (resolver problema)
8. ./submit.sh [nombre de notebook]
9. Hacer pull request
10. profit

```
