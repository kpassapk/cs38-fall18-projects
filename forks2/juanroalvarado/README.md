# My PySpark Projects

Este es mi /fork/ privado en Bitbucket del repo `kpassapk/cs38-fall18-projects`.

## Cómo correr los ejemplos localmente

Primero, seguir las instrucciones para el repositorio "kpassapk/vagrant-spark". Asegurar que el directorio de este proyecto está como un share, para poder editar archivos localmente. (Y, si prefieren Jupyter, los archivos locales quedar en el directorio local también, sobreviviendo un re-build de la máquina. Hay algunos to-dos para que esto funcione.

```

    $ vagrant plugin install vagrant-share
  
   $ git clone git@github.com:kpassapk/vagrant-spark
   $ cd vagrant-spark
   
```

Editar el Vagrantfile, cambiando el directorio de proyectos 

(actualmente `/Users/kyle/src/org/bitbucket/kpassapk/cs38-fall18-projects`) a el directorio donde está clonado _este repositorio_. Luego, todavía en el directorio de `vagrant-spark`:

```

    $ vagarant up
    $ vagrant ssh

```

El prompt verde debería decirles que están en Ubuntu Xenial, como usuario "vagrant". 

```
    $ pyspark
```

Copiar el código en `1_hello_world` a la consola. (Copy-paste.) El resultado debería ser el siguiente.

```
    3.14154496
``` 

Luego, correr Jupyter notebook (adentro de la máquina virtual.)

```
    $ cd /vagrant
    
    $ jupyter notebook --no-browser

    [I 12:37:32.809 NotebookApp] Writing notebook server cookie secret to /run/user/1000/jupyter/notebook_cookie_secret

    [I 12:37:33.231 NotebookApp] Serving notebooks from local directory: /home/vagrant
    [I 12:37:33.231 NotebookApp] The Jupyter Notebook is running at:
    [I 12:37:33.231 NotebookApp] http://localhost:8888/?token=c6a3be6b1a0e8fac5d7ae6aede45ab9569e1fdba8a2ca43c
    [I 12:37:33.231 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
    [C 12:37:33.232 NotebookApp]

        Copy/paste this URL into your browser when you connect for the first time,
        to login with a token:
            http://localhost:8888/?token=c6a3be6b1a0e8fac5d7ae6aede45ab9569e1fdba8a2ca43c
```

Ahora abrir un notebook de Python e ingresar el siguiente texto:

```
    import findspark
    findspark.init()

    import pyspark
    import random

    sc = pyspark.SparkContext(appName="Pi")
    num_samples = 100000000

    def inside(p):     
      x, y = random.random(), random.random()
      return x*x + y*y < 1

    count = sc.parallelize(range(0, num_samples)).filter(inside).count()

    pi = 4 * count / num_samples
    print(pi)

    sc.stop()

```

![screenshot](./img/screenshot_2018-07-30_07-53-21.png)

## Troubleshooting

A mi no me funcionó el share de Vagrant, entonces inicié un tunel SSH:

```
vagrant ssh-config > vagrant-ssh-config
ssh -L 8888:127.0.0.1:8888 -F vagrant-ssh-config default

```

## Grading

50%: pasa tests

50%: subjetivo

## Cómo hacer un submission


