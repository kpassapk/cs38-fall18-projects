# -*- coding: utf-8 -*-
"""
Created on Sun Sep 30 12:26:50 2018

@author: Sebastian
"""

import pandas as pd
import statsmodels.api as sm
import numpy as np 
from itertools import combinations 

df = pd.read_csv("cruise_ship_info.csv")

#print(df)

df = df.drop(['Ship_name', 'Cruise_line'], axis=1)

#print(df)


xVar = np.array(['Age', 'Tonnage', 'passengers', 'length', 'cabins', 'passenger_density'])


def calcVif(df, variables):
    for i in range(len(variables)):
        Y = df[variables[i]]
        xTemp = np.delete(variables,i) 
        X = df[xTemp]
        X = sm.add_constant(X)
        model = sm.OLS(Y,X).fit() 
        r = model.rsquared
        vif = 1/(1-r**2)
        print("The vif for ", variables[i], "is:", str(vif))

print(df.corr('pearson'))       

print("Calculating VIF for all independent variables: ")
calcVif(df, xVar)
print("\n")


xVar2 = np.array(['Tonnage', 'passengers', 'length', 'cabins', 'Age'])
print("Calculating VIF after filtering low correlation variables: ")
calcVif(df, xVar2)


print("\n")
print("Testing VIF with other combinations of variables: ")
xVar3 = np.array(['Age', 'Tonnage' ,'passengers', 'length', 'cabins'])

varCombinations = combinations(xVar3, 3)

for i in varCombinations:
    print("Calculating VIF for the following variables: ")
    print(i)
    calcVif(df, i)
    print("\n")
    
varCombinations2 = combinations(xVar3, 4)

for i in varCombinations2:
    print("Calculating VIF for the following variables: ")
    print(i)
    calcVif(df, i)
    print("\n")
    


'''
Possible models:
age, passengers, length
age, length, cabins
'''