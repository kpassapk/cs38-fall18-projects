#!/bin/bash

# Submission script
# Kyle Passarelli, kyle.passarelli@gmail.com

if [ $# -eq 0 ]
then
    echo "usage: submit.sh <notebook.ipynb>"
    exit 1
fi

submit_file=$1

submit_dir=$(dirname $submit_file)

upsteram_url=$(git remote show upstream | grep Fetch | awk '{print $3}')

my_url=$(git remote show origin | grep Fetch | awk '{print $3}')

my_user=$(echo $my_url | awk -F '/' '{print $4}')

tmpdir=/tmp/cs38-submissions

# submit_dir ...

full_submit_dir=$tmpdir/$submit_dir/$my_user

echo "
Submission will be at '$full_submit_dir'"

if [ ! -e $tmpdir ] ;then
    echo "
    Cloning project into $tmpdir..."
    git clone $my_url $tmpdir
else
    echo "
    Fetching new origin changes..."
    cd $tmpdir && git fetch && cd -
fi
echo "
Creating $full_submit_dir..."
mkdir -p $full_submit_dir

submit_md=submission-$(date '+%b%d-%Y-%H-%M-%S').md
echo "
Extracting '$submit_md' from $submit_file..."
jq ".cells[0].source" $submit_file | \
    sed '1d;$d' | \
    sed 's/"//g'| \
    sed 's/\\n//'g | \
    sed 's/,$//' \
        > $submit_dir/$submit_md
cp $submit_dir/$submit_md $full_submit_dir

cd $tmpdir

echo "
Checking out branch 'students/$my_user'"

current_branch=$(git rev-parse --abbrev-ref HEAD)
if [$current_branch != "students/$my_user" ]
then
    git checkout students/$my_user
    if [ $? ]; then
        echo "Branch 'students/$my_user' not found"
        exit 1
    fi
fi

echo "
Adding files..."

git add .

echo "
Committing files..."

git commit -m "$my_user $submit_dir submission for $(date '+%a %b %d %Y')"

echo "
Pushing files to 'students/$my_user'..."

git push -u origin students/$my_user
