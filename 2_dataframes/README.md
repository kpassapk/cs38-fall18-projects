# Project 2: Dataframes

Este directorio contiene un ejercicio, `Project_Exercise.ipynb`, así como otros notebooks dentro de `course_material` que cubren lo que vemos en la clase.

## Basics 

Archivo: `course_material/Basics.ipynb`

Lo básico con Spark, incluyendo:

- Empezar una sesión de Spark
- Cargar un archivo JSON
- Obtener los nombres de la scolumnas
- Ver el esquema de una tabla
- Seleccionar columnas
- Ver las primeras filas de un data frame
- Utillizar SQL en Spark

## Basic Operations

Archivo: `course_material/Basic_Ops.ipynb`

## Dates

Archivo:"`course_material/Dates.ipynb`

## GroupBy / Aggregate

Archivo:"`course_material/GroupBy_Aggregate.ipynb`

## Missing Data

Archivo:"`course_material/Missing_Data.ipynb`
